class TicTacToeGrid extends GenericGrid {

    constructor(options) {
        super(options)
        this.playerTurn = 'X'
    }
    togglePlayer() {
        this.playerTurn =
            (this.playerTurn === 'X') ? 'O' : 'X';
        //return this.playerTurn    //NO return needed.  will just change playerTurn property each time method is called.
    }
}