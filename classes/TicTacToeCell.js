class TicTacToeCell extends GenericCell {

    constructor(options) {
        super(options)
        this.player = null;
    }

    registerPlayerOwnership(player) {
        this.player = player;
    }
}