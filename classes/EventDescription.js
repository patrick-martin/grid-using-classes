class Event {

    constructor(eventListener, eventType, callback, context = null) {
        this.eventListener = eventListener
        this.eventType = eventType
        this.callback = context ? callback.bind(context) : callback //if context is supplied (if it is truthy), apply .bind method of callback to context.  Else, just apply callback function
        this.context = context
    }
}