 //let cell = new GenericCell (this.options) //need to create a Cell class... in a new file within our classes folder
 //rowElement.appendChild(cell.createCellElement(rowIndex, cellIndex))

 //Need to create GenericCell Class
 //needs to inherit the options object..
 //needs to contain a generateCellElement method
 class GenericCell {
     constructor(options) {
         this.options = options
         this.options.cellClasses = this.options.cellClasses || []
     }
     generateCellElement(rowIndex, cellIndex) {
         this.element = document.createElement('div');
         this.element.style.width = this.options.cellWidth;
         this.element.style.height = this.options.cellHeight;
         this.addStyleClass('cell', ...this.options.cellClasses); //we want to add another class-->as in checkers, red class and a black class.  *adding the ... creates a string out of an array.  It is called a 'spread operator'
         this.element.dataset.rowIndex = rowIndex;
         this.element.dataset.columnIndex = columnIndex;

         return this.element;

     }
     addStyleClass(...classes) { //Called a 'rest operator'.  Different than a 'spread operator'.  It sucks up all of the contents and adds them as classes
         this.element.classList.add(...classes)
     }

 }