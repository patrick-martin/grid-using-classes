//Prototype Inheritance or use Class syntax?
//Let's do Class syntax!

class GenericGrid {

    constructor(options = { //creating options object; storing options object within the grid
            rowCount: 10,
            columnCount: 10,
            cellWidth: '10px',
            cellHeight: '10px',

        }) {
            this.options = options; //calling on 'options' object

        }
        //creating our method
    generateGridElement() { //set of nested for-loops that creates rows/columns.  Outer loop is row, Inner loop is the cells/columns
            this.element = document.createElement('div');
            this.element.id = 'grid';

            for (let rowIndex = 0; rowIndex < this.options.rowCount; rowIndex++) {
                let rowElement = this.createRowElement(rowIndex); //refactored below
                //rowElement.classList.add('row'); //refactored below
                this.element.appendChild(this.createRowElement(rowIndex)); //appending our new rowElement to this.element created above
                //rowElement.dataset.rowIndex = rowIndex; //refactored below

                for (let cellIndex = 0; cellIndex < this.options.columnCount; cellIndex++) {
                    let cell = new GenericCell(this.options) //need to create a Cell class... in a new file within our classes folder
                    rowElement.appendChild(cell.createCellElement(rowIndex, cellIndex))
                        //createCellElement vs. generateCellElement

                }
            }
            return this.element
        }
        //createRowElement method that will actually create an element.
    createRowElement(rowIndex) {
            const element = document.createElement('div')
            element.classList.add('row')
            element.dataset.rowIndex = rowIndex;

            return element
        }
        //creating our addEventListeners method --> using a 'for..of' loop to loop through our eventListeners array to get our descriptions
        //using 'destructuring' to grab these properties and assign them to their respective labels
        //was for (let eventDescriptions of eventDescription)
    addEventListeners(eventDescription) {
        for (let { eventListener, eventType, callback }
            of eventDescriptions) {
            eventListener.addEventListener(eventType, callback)
        }
    }
}