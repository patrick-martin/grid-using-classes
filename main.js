const gridContainer = document.querySelector('#grid-container')

const grid = new TicTacToeGrid({
    rowCount: 3,
    columnCount: 3,
    cellWidth: '180px',
    cellHeight: '180px',
    cellClasses: ['red'],

})

const gridElement = grid.generateGridElement()
gridContainer.appendChild(gridElement)

const eventListeners = [
    new EventDescription({
        listener: gridElement,
        eventType: 'click',
        callback: function(event) {
            console.log('A click has occurred on ' + event.target + '.')
            console.log(`'this' refers to ${grid}`)
        },
        context: grid
    }),

]

grid.addEventListeners(eventListeners)